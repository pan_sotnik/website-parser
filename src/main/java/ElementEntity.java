//A kind of entity class
public class ElementEntity {

    private String name;
    private String brand;
    private String color;
    private String price;
    private String description;
    private String isbn;

    public ElementEntity(String name, String brand, String color, String price, String description, String isbn) {
        this.name = name;
        this.brand = brand;
        this.color = color;
        this.price = price;
        this.description = description;
        this.isbn = isbn;
    }


    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public String getColor() {
        return color;
    }

    public String getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getIsbn() {
        return isbn;
    }
}
