import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.text.NumberFormat;
import java.util.*;


public class Main {

    public static void main(String[] args) {

        long start = System.currentTimeMillis();

        //Begin by gathering memory usage statistics
        Runtime runtime = Runtime.getRuntime();
        NumberFormat format = NumberFormat.getInstance();

        StringBuilder sb = new StringBuilder();
        long maxMemory = runtime.maxMemory();
        long allocatedMemory = runtime.totalMemory();
        long freeMemory = runtime.freeMemory();
        sb.append("free memory: ").append(format.format(freeMemory / 1024)).append(". ");
        sb.append("allocated memory: ").append(format.format(allocatedMemory / 1024)).append(". ");
        sb.append("max memory: ").append(format.format(maxMemory / 1024)).append(". ");
        sb.append("total free memory: ").append(format.format((freeMemory + (maxMemory - allocatedMemory)) / 1024)).append(". ");

        //The latest version of Selenium requires us to specify path to executable.
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\P@n_$otnik\\Desktop\\Stuff\\Dev stuff\\geckodriver.exe");

        WebDriver primaryDriver = new FirefoxDriver();

        //I have decided it were best we selected a gender straight away.
        System.out.println("You are searching for: " + args[0] + ". Enter 'men', 'women' or 'children'.");


        String gender = new Scanner(System.in).nextLine();

        if (gender.equals("men")){
            primaryDriver.get("https://www.aboutyou.de/suche?term="+args[0]+"&category=20202");
        } else if (gender.equals("women")){
            primaryDriver.get("https://www.aboutyou.de/suche?term="+args[0]+"&category=20201");
        } else if (gender.equals("children")){
            primaryDriver.get("https://www.aboutyou.de/suche?term="+args[0]+"&category=138113");
        } else {
            System.out.println("Please pick one of the above categories.");
            primaryDriver.quit();
            main(args);
        }

        new TaskRunner().runTask(primaryDriver);

        long end = System.currentTimeMillis();

        System.out.println("Memory usage statistics are as follows: " + sb);

        System.out.println("The program has completed in " + (end - start) + " milliseconds.");

}

}
