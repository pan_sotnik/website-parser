import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class Parser {

    private static List<ElementEntity> collection = new ArrayList<ElementEntity>();

    //Synchronized and static so all threads have access to the same object
    public synchronized static void addToCollection(ElementEntity collectionToAdd){
        collection.add(collectionToAdd);
    }

    public static int printStatus(){
        return collection.size();
    }

    public static void beginParse() {

try{

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    Document document = builder.newDocument();

    Element root = document.createElement("offers");
    document.appendChild(root);

    for (int i=0; i<=collection.size(); i++){

        Element item = document.createElement("offer");
        root.appendChild(item);

        Element name = document.createElement("name");
        name.setTextContent(collection.get(i).getName());
        item.appendChild(name);

        Element brand = document.createElement("brand");
        brand.setTextContent(collection.get(i).getBrand());
        item.appendChild(brand);

        Element color = document.createElement("color");
        color.setTextContent(collection.get(i).getColor());
        item.appendChild(color);

        Element price = document.createElement("price");
        price.setTextContent(collection.get(i).getPrice());
        item.appendChild(price);

        Element description = document.createElement("description");
        description.setTextContent(collection.get(i).getDescription());
        item.appendChild(description);

        Element isbn = document.createElement("isbn");
        isbn.setTextContent(collection.get(i).getIsbn());
        item.appendChild(isbn);

    }

    TransformerFactory transformFactory = TransformerFactory.newInstance();
    Transformer transformer = transformFactory.newTransformer();

    DOMSource source = new DOMSource(document);
    StreamResult result = new StreamResult(new File(System.getProperty("user.home") + "/Collection Result.xml"));

    transformer.transform(source, result);

} catch (Exception e){
    e.printStackTrace();
}

        System.out.println(printStatus() + " records have been parsed.");
        System.out.println("Please check your user directory for the file.");

    }


}
