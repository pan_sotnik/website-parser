import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Scraper implements Runnable {

    private WebElement toParse;
    private int pageCount;
    private WebDriver driver;


    public Scraper(WebElement toParse, int pageCount, WebDriver driver) {
        this.toParse = toParse;
        this.pageCount = pageCount;
        this.driver = driver;
    }

    public void run() {

        driver.navigate().to(toParse.getAttribute("href"));

        String name = driver.findElement(By.xpath("//meta[@property='og:title']")).getAttribute("content");

        String brand = driver.findElement(By.xpath("//meta[@property='og:brand']")).getAttribute("content");

        String color = driver.findElement(By.xpath("//meta[@property='product:color']")).getAttribute("content");

        String price = driver.findElement(By.xpath("//meta[@property='og:price:amount']")).getAttribute("content");

        String description = driver.findElement(By.xpath("//meta[@property='og:description']")).getAttribute("content");

        String isbn = driver.findElement(By.xpath("//meta[@property='og:isbn']")).getAttribute("content");

        System.out.println("Parsing record: " + color + " " + price + " " + brand + " " + name + " " + isbn + " " + description);

        Parser.addToCollection(new ElementEntity(name, brand, color, price, description, isbn));

        System.out.println(Thread.currentThread().getName() + " has completed..." + "page is " + pageCount);


    }






}
