import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TaskRunner {

    private int pageCount = 1;
    private List<WebElement> pageContent = new ArrayList<WebElement>();

    //References to these WebDriver objects are passed to every thread to reuse open browser windows
    WebDriver[] drivers = {new FirefoxDriver(), new FirefoxDriver(), new FirefoxDriver(), new FirefoxDriver()};

    //This variable is used to determine which Driver object to assign a thread
    private int driverAssignIndex = 0;

public void runTask(WebDriver primaryDriver){

        pageContent.addAll(primaryDriver.findElements(By.xpath("//a[contains(@href, 'p')][@class='product-name-link']")));

        System.out.println("There are " + pageContent.size() + " elements on the inspected page.");

        //A maximum of 98 elements per page to be processed in 4 threads
        ExecutorService execution = Executors.newFixedThreadPool(4);

        for (WebElement anItem : pageContent) {
            if (driverAssignIndex == 4){ driverAssignIndex = 0;}
        execution.submit(new Scraper(anItem, pageCount, drivers[driverAssignIndex]));
        driverAssignIndex++;
        }

        execution.shutdown();

        try {
            execution.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        pageContent.clear();
        pageCount++;
        driverAssignIndex = 0;

        //Go to next page or return when last page reached
         try {
          primaryDriver.findElement(By.partialLinkText("Nächste Seite")).click();
        } catch (NoSuchElementException e){
            System.out.println("The last page has been reached.");
            System.out.println("XML file is being created.");
            Parser.beginParse();
            return;
        }

        //Run method recursively
        runTask(primaryDriver);

}


}
